# frozen_string_literal: true

require_relative "r00lz/version"

module R00lz
  class Error < StandardError; end

  class App
    def call(env)
      kl, act = cont_and_act(env) # class, action, controller
      text = kl.new(env).send(act)
      [200, 
        {'Content-Type' => 'text/hteml'},
        [text]]
    end

    def cont_and_act(env)
      _, con, act, after = env["PATH_INFO"].split('/') # controller, action
      con = con.capitalize + "Controller"
      [Object.const_get(con), act]
    end
  end

  require "erb" # NOTE: this is the ERB (embedded ruby) implementation that is built into ruby (there are other implementations if you want)
  class Controller
    attr_reader :env

    def initialize(env)
      STDERR.puts("MISSY LOOK env = #{env}") # NOTE: good way to print out things to the server to verify what you're looking at 
      @env = env
    end

    def render(name, b = binding()) # b = binding(): default is to call binding wherever the render is called
      template = "app/views/#{name}.html.erb"
      e = ERB.new(File.read template)
      e.result(b)
    end

    def request
      @request ||= Rack::Request.new @env # NOTE: memoize/cache the request, @env is the environment variables rack originally passed in when the request started up
    end

    def params
      request.params
    end
  end

  def self.to_underscore(s) # NOTE: convert to camel case (same code as in Rails) 
    s.gsub(
      /([A-Z]+)([A-Z][a-z])/,
      '\1_\2').gsub(
      /([a-z\d])([A-Z])/,
      '\1_\2').downcase
  end
end

class Object
  def self.const_missing(c) # NOTE: here we're overriding the const_missing method to look for something we want
    require R00lz.to_underscore(c.to_s) # NOTE: c comes in as a symbol, so we need it as a string
    Object.const_get(c)
  end
end

class FileModel
  def initialize(fn) # filename
    @fn = fn
    cont = File.read fn 
    STDERR.puts("MISSY LOOK content read from file: #{cont}")
    @hash = JSON.load cont
  end

  def [](field) # NOTE: override [] operation in Ruby 
    @hash[field.to_s]
  end

  def self.find(id)
    self.new "data/#{id}.json" # NOTE: creates filename and passes to self.new
  rescue
    nil
  end
end