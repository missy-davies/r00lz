# frozen_string_literal: true

require_relative "lib/r00lz/version"

Gem::Specification.new do |spec|
  spec.name = "r00lz"
  spec.version = R00lz::VERSION
  spec.authors = ["Missy Davies"]
  spec.email = ["ms.melissadavies@gmail.com"]

  spec.summary = "Rebuilding rails tutorial."
  spec.description = "Working through video chapters of rebuilding rails tutorials."
  spec.homepage = "https://gitlab.com/missy-davies/r00lz"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["allowed_push_host"] = "https://example.com"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/missy-davies/r00lz"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # NOTE: Ruby's interface to web servers (puma, falcon, etc). A rack endpoint acts like a proc. It calls #call with request info and the value is sent back.
  # NOTE: We'll be writing to Rack interface, rather than going through Rails or another existing framework, we'll write out own 
  # NOTE: This should be a runtime dependency, not a development dependency
  spec.add_runtime_dependency "rack", "~>2.0.7"
  spec.add_runtime_dependency "json", "~>2.1.0"

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
